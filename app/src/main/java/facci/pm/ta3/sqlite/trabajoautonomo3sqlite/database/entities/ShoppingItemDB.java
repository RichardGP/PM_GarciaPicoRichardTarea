package facci.pm.ta3.sqlite.trabajoautonomo3sqlite.database.entities;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

import facci.pm.ta3.sqlite.trabajoautonomo3sqlite.MainActivity;
import facci.pm.ta3.sqlite.trabajoautonomo3sqlite.database.helper.ShoppingElementHelper;
import facci.pm.ta3.sqlite.trabajoautonomo3sqlite.database.model.ShoppingItem;
import java.util.ArrayList;

public class ShoppingItemDB {

    private static final String TEXT_TYPE = " TEXT";
    private static final String COMMA_SEP = ",";

    private ShoppingElementHelper dbHelper;

    public ShoppingItemDB(Context context) {
        // Create new helper
        dbHelper = new ShoppingElementHelper(context);
    }

    /* Inner class that defines the table contents */
    public static abstract class ShoppingElementEntry implements BaseColumns {
        public static final String TABLE_NAME = "entry";
        public static final String COLUMN_NAME_TITLE = "title";

        public static final String CREATE_TABLE = "CREATE TABLE " +
                TABLE_NAME + " (" +
                _ID + " INTEGER PRIMARY KEY AUTOINCREMENT" + COMMA_SEP +
                COLUMN_NAME_TITLE + TEXT_TYPE + " )";

        public static final String DELETE_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;
    }


    public void insertElement(String productName) {
        //TODO: Todo el código necesario para INSERTAR un Item a la Base de datos
        // Obtiene el repositorio de datos insertado en modo escritura
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        //Se crea una instancia de la clase ContentValues la cual se utiliza para almacenar un conjunto de valores
        ContentValues values = new ContentValues();
        //Crea a nuevo mapa de valores que se almacenaran en values, y  le pasamos como parametros
        //al metodo put el nombre de la columna y los productos que vamos a insertar
        values.put(ShoppingElementEntry.COLUMN_NAME_TITLE, productName);

// Insert the new row, returning the primary key value of the new row
        //Inserta la nueva fila, retornando la clave primaria "value" de la nueva fila
        //A traves del db.insert se inserta los valores capturados anteriormente en la tabla,
        //se pasa el nombre para identificar a que tabla perteneceran los datos insertados
        long newRowId = db.insert(ShoppingElementEntry.TABLE_NAME, null, values);
    }


    public ArrayList<ShoppingItem> getAllItems() {

        ArrayList<ShoppingItem> shoppingItems = new ArrayList<>();

        String[] allColumns = { ShoppingElementEntry._ID,
            ShoppingElementEntry.COLUMN_NAME_TITLE};

        Cursor cursor = dbHelper.getReadableDatabase().query(
            ShoppingElementEntry.TABLE_NAME,    // The table to query
            allColumns,                         // The columns to return
            null,                               // The columns for the WHERE clause
            null,                               // The values for the WHERE clause
            null,                               // don't group the rows
            null,                               // don't filter by row groups
            null                                // The sort order
        );

        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            ShoppingItem shoppingItem = new ShoppingItem(getItemId(cursor), getItemName(cursor));
            shoppingItems.add(shoppingItem);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        dbHelper.getReadableDatabase().close();
        return shoppingItems;
    }

    private long getItemId(Cursor cursor) {
        return cursor.getLong(cursor.getColumnIndexOrThrow(ShoppingElementEntry._ID));
    }

    private String getItemName(Cursor cursor) {
        return cursor.getString(cursor.getColumnIndexOrThrow(ShoppingElementEntry.COLUMN_NAME_TITLE));
    }


    public void clearAllItems() {
        //TODO: Todo el código necesario para ELIMINAR todos los Items de la Base de datos
        // Obtiene el repositorio de datos insertado en modo escritura
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        //El metodo delete nos permite eliminar datos de la tabla
        //Le pasamos como paremetro el nombre de la tabla y los demas parametros en null
        //para que eliminen todos los datos de la tabla, sin hacer una seleccion de un dato especifico
        db.delete(ShoppingElementEntry.TABLE_NAME, null, null);


    }

    public void updateItem(ShoppingItem shoppingItem) {
        //TODO: Todo el código necesario para ACTUALIZAR un Item en la Base de datos
        // Obtiene el repositorio de datos insertado en modo escritura
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        //Nuevo valor para una columna
        ///Se obtiene el nombre de los nuevos item a actualizar y se asigna a la variable title
        String title = shoppingItem.getName();
        //Se crea una instancia de la clase ContentValues la cual se utiliza para almacenar un conjunto de valores
        ContentValues values = new ContentValues();
        //Crea a nuevo mapa de valores que se almacenaran en values, y  le pasamos como parametros
        //al metodo put el nombre de la columna y los productos que vamos a actualizar
        values.put(ShoppingElementEntry.COLUMN_NAME_TITLE, title);
        // Which row to update, based on the title
        //Se establece que fila actualizar basandose en las coincidencias
        //de id
        String selection = ShoppingElementEntry._ID+ " LIKE ?";
        //Describe los argumentos en el orden mostrado por el array de string
        String[] selectionArgs = { String.valueOf(shoppingItem.getId()) };
       //Se actualizan los items seleccionados en la tabla
        int count = db.update(
                ShoppingItemDB.ShoppingElementEntry.TABLE_NAME,
                values,
                selection,
                selectionArgs);

    }

    public void deleteItem(ShoppingItem shoppingItem) {
        //TODO: Todo el código necesario para ELIMINAR un Item de la Base de datos
        // Obtiene el repositorio de datos insertado en modo escritura
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        //Define donde se realiza la consulta , y con el operador LIKE se busca las
        //columnas que concuerdan con los valores dados
        String selection = ShoppingElementEntry.COLUMN_NAME_TITLE + " LIKE ?";
        // Specify arguments in placeholder order.
        //Describe los argumentos en el orden mostrado por el array de string
        String[] selectionArgs = { shoppingItem.getName() };
        //Se emite la instruccion SQL para que se eliminen los elementos seleccionados de la tabla
        //en la posicion establecida
        int deletedRows = db.delete(ShoppingElementEntry.TABLE_NAME, selection, selectionArgs);
    }

}
